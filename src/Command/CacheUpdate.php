<?php

namespace App\Command;

use App\Entity\Rss;
use App\Model\DAO\NewsDAO;
use App\Model\DAO\RssDAO;
use App\Model\Service\FeedBo;
use App\Model\Service\RssBo;
use DateTime;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CacheUpdate extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'redis:cache:update';

    /**
     * @var RssDAO
     */
    private $rssDAO;

    /**
     * @var FeedBo
     */
    private $feedBo;

    /**
     * @var RedisAdapter
     */
    private $redis;

    /**
     * @var NewsDAO
     */
    private $newsDAO;

    /**
     * CacheUpdate constructor.
     * @param RssDAO $rssDAO
     * @param FeedBo $feedBo
     * @param NewsDAO $newsDAO
     * @param null $name
     */
    public function __construct(
        RssDAO $rssDAO,
        FeedBo $feedBo,
        NewsDAO $newsDAO,
        $name = null
    )
    {
        parent::__construct($name);
        $this->rssDAO = $rssDAO;
        $this->feedBo = $feedBo;
        $this->newsDAO = $newsDAO;
        $this->redis = RedisAdapter::createConnection('redis://127.0.0.1:6379');
    }

    protected function configure()
    {
        $this->setDescription('Update redis cache');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->redis->select(1);
        $rssDos = $this->rssDAO->selectAll();
        foreach ($rssDos as $rss) {
            $lastModified = new DateTime();
            $lastModified->setTimestamp($rss->getLastModified());

            $newsDOs = $this->newsDAO->selectAllFromRss($rss, $lastModified);
            $this->redis->set("RSS_FEED_" . $rss->getId(), serialize($newsDOs));
        }
    }
}


<?php

namespace App\Entity;

/**
 * Class AbstractEntity
 * @package App\Entity
 */
class AbstractEntity
{

    public function __construct($data = null)
    {
        if(is_array($data)){
            $this->hydrate($data);
        }
    }

    /**
     * Hydrate entity with array of data
     *
     * @param $data
     */
    public function hydrate($data)
    {
        foreach ($data as $attr => $value) {
            $potential_method = 'set' . lcfirst($attr);
            if (method_exists($this, $potential_method)) {
                $this->$potential_method($value);
            }
        }
    }
}

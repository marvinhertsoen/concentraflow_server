<?php

namespace App\Model\DAO;

use App\Entity\Category;
use App\Entity\News;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use FeedIo\FeedIo;
use Symfony\Component\Cache\Adapter\RedisAdapter;

class NewsDAO extends AbstractDAO
{

    /**
     * @var FeedIo
     */
    private $feedIo;

    /**
     * @var \Predis\ClientInterface|\Redis|\RedisCluster
     */
    private $redis;

    public function __construct(EntityManagerInterface $em, FeedIo $feedIo)
    {
        parent::__construct($em);
        $this->feedIo = $feedIo;
        $this->redis = RedisAdapter::createConnection('redis://127.0.0.1:6379');
    }

    /**
     * Select all News DO from Rss feed
     *
     * @param $fluxUrl
     * @param null $since
     * @return array
     */
    public function selectAllFromRss($rss, $since = null): array
    {
        $result = [];
        try {
            $IoFeed = $this->feedIo->read($rss->getUrl())->getFeed();
        } catch (\Exception $e) {
            return $result;
        }

        foreach ($IoFeed as $rssNews) {
            $date = $rssNews->getLastModified();
            $newsEntity = new News();
            $newsEntity->setTitle($rssNews->getTitle());
            $newsEntity->setLink($rssNews->getLink());
            $newsEntity->setDescription($rssNews->getDescription());
            $newsEntity->setImage((isset($rssNews->getMedias()[0])) ? $rssNews->getMedias()[0]->getUrl() : null);
            $newsEntity->setPubDate($date ? $date->getTimestamp() : 0);
            $newsEntity->setRss($rss);
            $result[] = $newsEntity;
        }
        return $result;
    }

    public function selectAllFromCache($rssId): array
    {
        $result = [];
        try {
            $this->redis->select(1);
            $newsDOs = unserialize($this->redis->get('RSS_FEED_' . $rssId));
            if(!is_array($newsDOs)){
                return $result;
            }
        } catch (\Exception $e) {
            return $result;
        }

        return $newsDOs;
    }

    /**
     * Tell if a date is outdated compared to "now since X seconds".
     *
     * @param int $dateNow
     * @param int|null $since
     * @param int $date
     * @return bool
     */
    public function isOutdated(int $dateNow, ?int $since, int $date): bool
    {
        if ($since) {
            $gap = $dateNow - $since;
            return ($date < $gap) ? true : false;
        }
        return false;
    }
}

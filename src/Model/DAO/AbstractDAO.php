<?php

namespace App\Model\DAO;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;

/**
 * Abstract DAO
 *
 * Class AbstractDAO
 * @package App\Model\DAO
 */
abstract class AbstractDAO
{

    /**
     * @var string
     */
    protected $entityClass;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * Constructor
     *
     * AbstractDAO constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->entityClass = $this->extractEntityClassName();
        $this->em = $em;
    }

    /**
     * Select All Entities
     *
     * @return array
     */
    public function selectAll(): array
    {
        return $this->em->getRepository($this->entityClass)->findAll();
    }

    /**
     * Select one Entity
     *
     * @param $id
     * @return Mixed
     */
    public function select($id)
    {
        return $this->em->find($this->entityClass, $id);
    }

    /**
     * Find Entity/ies by characteristics
     *
     * @param $data
     * @return Mixed
     */
    public function find($data)
    {
        return $this->em->getRepository($this->entityClass)->findBy($data);
    }

    /**
     * Create an Entity
     *
     * @param $entity
     * @return mixed
     */
    public function insert($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
        return ($entity->getId()) ? $entity : null;
    }

    /**
     * Delete an Entity
     *
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $entity = $this->em->find($this->entityClass, $id);
        if ($entity) {
            $this->em->remove($entity);
            $this->em->flush();
            return true;
        }
        return false;
    }

    /**
     * Update an Entity
     *
     * @param $id
     * @param $entityToUpdate
     * @return bool|mixed|object|null
     */
    public function update($id, $entityToUpdate)
    {
        if ($entityToUpdate) {
            $this->em->flush();
        }
        return $entityToUpdate ? $entityToUpdate : null;
    }


    /**
     * Return Entity class name from "concrete" DAO Class Name
     * ex : CategoryDAO -> App\Entity\Category
     *
     * @return string
     */
    public function extractEntityClassName()
    {
        preg_match('/\\\\(\w+)DAO/', get_called_class(), $matches);
        return ('App\Entity\\' . $matches[1]);
    }

    /**
     * Hydrate an entity passed as reference with data array
     *
     * @param $entity
     * @param $data
     * @return mixed
     */
    public function hydrate(&$entity, $data)
    {
        foreach ($data as $attr => $value) {
            $potential_method = 'set' . lcfirst($attr);
            if (method_exists($entity, $potential_method)) {
                $entity->$potential_method($value);
            }
        }
        return $entity;
    }

}


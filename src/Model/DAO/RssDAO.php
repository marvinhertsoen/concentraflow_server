<?php

namespace App\Model\DAO;

use App\Entity\Category;
use App\Entity\Rss;
use Doctrine\ORM\Mapping\Entity;

class RssDAO extends AbstractDAO
{

    public function search($term)
    {
        $json = file_get_contents('https://cloud.feedly.com/v3/search/feeds?query=' . $term);
        $result = json_decode($json);

        $rssDOs = [];
        foreach ($result->results as $feed) {
            $rss = new Rss();
            $rss->setName(isset($feed->title) ? $feed->title : null);

            preg_match('/^feed\/(.*)$/', $feed->id, $matches);
            $rss->setUrl($matches[1]);

            $rss->setDescription(isset($feed->description) ? $feed->description : null);
            $rss->setLanguage(isset($feed->language) ? $feed->language : null);
            $rss->setWebsite(isset($feed->website) ? $feed->website : null);
            $rssDOs[] = $rss;
        }
        return $rssDOs;
    }
}

<?php

namespace App\Model\Service;

use App\Entity\News;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;


class ReadRssBo
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ReadRssBo constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Read an rss feed & return array of newsEntities from the rss items
     *
     * @param $flux
     * @return array
     */
    public function read($flux): array
    {
        $rss_feed = simplexml_load_file($flux);
        $result = [];

        foreach ($rss_feed->channel->item as $news) {
            $newsEntity = new News();
            $newsEntity->setId("1");
            $newsEntity->setTitle($news->title);
            $newsEntity->setLink($news->link);
            $newsEntity->setDescription($news->description);
            $newsEntity->setImage($news->enclosure['url']);

            $date = new DateTime($news->pubDate->__toString());
            $newsEntity->setPubDate($date->getTimestamp());

            //$newsEntity
            $result[] = $newsEntity;
        }
        return $result;
    }
}

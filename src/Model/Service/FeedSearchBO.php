<?php

namespace App\Model\Service;

use App\Entity\Category;
use App\Entity\Rss;
use App\Entity\News;
use App\Model\DAO\CategoryDAO;
use App\Model\DAO\NewsDAO;
use App\Model\DAO\RssDAO;
use App\Model\DTO\FeedDTO;
use App\Model\DTO\NewsDTO;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\HttpFoundation\Response;

class FeedSearchBO
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var CategoryDAO
     */
    private $rssDAO;

    /**
     * FeedSearchBO constructor.
     * @param EntityManagerInterface $em
     * @param RssDAO $rssDAO
     */
    public function __construct(
        EntityManagerInterface $em,
        RssDAO $rssDAO
    )
    {
        $this->em = $em;
        $this->rssDAO = $rssDAO;
    }

    /**
     * Search rss feeds from term
     *
     * @return FeedDTO
     * @throws \Exception
     */
    public function search($term)
    {

        $rssDOs = $this->rssDAO->search($term);
        foreach ($rssDOs as $rss) {
            $followed = $this->rssDAO->find(['url' => $rss->getUrl()]);
            if ($followed) {
                $rss->setId($followed[0]->getId());
            }
        }
        return $rssDOs;
    }
}

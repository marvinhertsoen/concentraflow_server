<?php

namespace App\Model\Service;

use App\Entity\Category;
use App\Entity\Rss;
use App\Entity\News;
use App\Model\DAO\CategoryDAO;
use App\Model\DAO\RssDAO;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\HttpFoundation\Response;


class RssBo
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Response
     */
    private $response;
    /**
     * @var CategoryDAO
     */
    private $dao;
    /**
     * @var CategoryDAO
     */
    private $categoryDAO;

    /**
     * RssBo constructor.
     * @param EntityManagerInterface $em
     * @param RssDAO $dao
     * @param CategoryDAO $categoryDAO
     */
    public function __construct(
        EntityManagerInterface $em,
        RssDAO $dao,
        CategoryDAO $categoryDAO
    )
    {
        $this->em = $em;
        $this->dao = $dao;
        $this->categoryDAO = $categoryDAO;
    }


    /**
     * Get all Rss
     *
     * @return array
     */
    public function getAll(): array
    {
        return $this->dao->selectAll();
    }

    /**
     * Get all Rss
     *
     * @return mixed
     */
    public function get($id)
    {
        return $this->dao->select($id);
    }

    /**
     * Create a Rss Entity
     *
     * @param $data
     * @return bool / Entity
     */
    public function create($data)
    {
        $rss = new Rss($data);
        $category = $this->extractCategory($rss);
        $rss->setCategory($category);
        return $this->dao->insert($rss);
    }

    /**
     * Update a Rss Entity
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    public function edit($id, $data)
    {
        $rss = $this->dao->select($id);
        if($rss){
            $rss->hydrate($data);
        }
        $category = $this->extractCategory($rss);
        $rss->setCategory($category);
        return $this->dao->update($id, $rss);
    }


    /**
     * Remove a Rss Entity
     *
     * @param $id
     * @return bool
     */
    public function remove($id): bool
    {
        return $this->dao->delete($id);
    }


    private function extractCategory($rss)
    {
        $rssCateg = $rss->getCategory();
        if (!$rssCateg) {
            return null;
        }

        $category = $this->categoryDAO->select($rssCateg);
        if (!$category) {
            $category = new Category(["name" => $rssCateg]);
            $category = $this->categoryDAO->insert($category);
        }
        return $category;
    }

}

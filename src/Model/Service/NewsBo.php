<?php

namespace App\Model\Service;

use App\Model\DAO\NewsDAO;
use App\Model\DAO\RssDAO;
use Doctrine\ORM\EntityManagerInterface;


class NewsBo
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * FeedBo constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    )
    {
        $this->em = $em;
    }
}

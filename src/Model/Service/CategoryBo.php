<?php

namespace App\Model\Service;

use App\Entity\Category;
use App\Entity\Rss;
use App\Entity\News;
use App\Model\DAO\CategoryDAO;
use App\Model\DAO\RssDAO;
use App\Model\DTO\CategoryDTO;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\HttpFoundation\Response;


class CategoryBo
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var CategoryDAO
     */
    private $dao;

    /**
     * @var RssDAO
     */
    private $rssDAO;

    /**
     * CategoryBo constructor.
     * @param EntityManagerInterface $em
     * @param CategoryDAO $dao
     * @param RssDAO $rssDAO
     */
    public function __construct(
        EntityManagerInterface $em,
        CategoryDAO $dao,
        RssDAO $rssDAO
    )
    {
        $this->em = $em;
        $this->dao = $dao;
        $this->rssDAO = $rssDAO;
    }


    /**
     * Get all Categories
     *
     * @return array
     */
    public function getAll(): array
    {
        $categoriesDOs = $this->dao->selectAll();
        $categoriesDTOs = [];

        foreach($categoriesDOs as $categoriesDO){
            $categoriesDTO = new CategoryDTO();
            $categoriesDTO->setId($categoriesDO->getID());
            $categoriesDTO->setName($categoriesDO->getName());

            $rssDOs = $this->rssDAO->find(['category' => $categoriesDO]);

            //sort rss entities found by name
            usort($rssDOs , 'self::comparator');

            $categoriesDTO->setRss($rssDOs);

            $categoriesDTOs[] = $categoriesDTO;
        }
        return $categoriesDTOs;
    }

    /**
     * Create a Category Entity
     *
     * @param $data
     * @return bool / Entity
     */
    public function create($data)
    {
        $category = new Category($data);
        return $this->dao->insert($category);
    }

    /**
     * Update a Category Entity
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    public function edit($id, $data)
    {
        return $this->dao->update($id, $data);
    }

    /**
     * Remove a Category Entity
     *
     * @param $id
     * @return bool
     */
    public function remove($id): bool
    {
        return $this->dao->delete($id);
    }

    // Comparator function used for order news
    function comparator($dto1, $dto2)
    {
        return strnatcasecmp($dto1->getName(),$dto2->getName());
    }
}

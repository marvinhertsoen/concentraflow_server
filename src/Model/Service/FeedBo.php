<?php

namespace App\Model\Service;

use App\Entity\Category;
use App\Entity\Rss;
use App\Entity\News;
use App\Model\DAO\CategoryDAO;
use App\Model\DAO\NewsDAO;
use App\Model\DAO\RssDAO;
use App\Model\DTO\FeedDTO;
use App\Model\DTO\NewsDTO;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\HttpFoundation\Response;


class FeedBo
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var CategoryDAO
     */
    private $rssDAO;

    /**
     * @var NewsDAO
     */
    private $newsDAO;

    /**
     * FeedBo constructor.
     * @param EntityManagerInterface $em
     * @param RssDAO $rssDAO
     * @param NewsDAO $newsDAO
     */
    public function __construct(
        EntityManagerInterface $em,
        RssDAO $rssDAO,
        NewsDAO $newsDAO
    )
    {
        $this->em = $em;
        $this->rssDAO = $rssDAO;
        $this->newsDAO = $newsDAO;
    }


    /**
     * Get Feed from all Rss Feeds
     *
     * @return FeedDTO
     * @throws \Exception
     */
    public function getAll(): FeedDTO
    {
        $feedDTO = new FeedDTO();
        $rssDOs = $this->rssDAO->selectAll();

        foreach ($rssDOs as $rssDO) {
            //$newsDOs= $this->newsDAO->selectAllFromRss($rssDO->getUrl());
            $newsDOs = $this->newsDAO->selectAllFromCache($rssDO->getId());
            $newsDTOs = $this->morph($rssDO, $newsDOs);
            $feedDTO->news = array_merge($feedDTO->news, $newsDTOs);
        }
        usort($feedDTO->news, 'self::comparator');
        return $feedDTO;
    }

    /**
     * Get Feed from single Rss Feed
     *
     * @return FeedDTO
     * @throws \Exception
     */
    public function get($id): ?FeedDTO
    {

        $feedDTO = new FeedDTO();
        $rssDO = $this->rssDAO->select($id);

        if ($rssDO) {
            $newsDOs = $this->newsDAO->selectAllFromCache($rssDO->getId());
            $newsDTOs = $this->morph($rssDO, $newsDOs);
            $feedDTO->news = array_merge($feedDTO->news, $newsDTOs);

            //Sort news by date decreasing
            usort($feedDTO->news, 'self::comparator');
            return $feedDTO;
        }
        return null;
    }

    /**
     * Get Feed from a Category
     *
     * @return FeedDTO
     * @throws \Exception
     */
    public function getFromCategory($id): ?FeedDTO
    {
        $feedDTO = new FeedDTO();
        $rssDOs = $this->rssDAO->find(["category" => $id]);
        foreach ($rssDOs as $rssDO) {
            $newsDOs = $this->newsDAO->selectAllFromCache($rssDO->getId());
            $newsDTOs = $this->morph($rssDO, $newsDOs);
            $feedDTO->news = array_merge($feedDTO->news, $newsDTOs);
        }
        usort($feedDTO->news, 'self::comparator');
        return $feedDTO;
    }


    private function morph($rssDO, $newsDOs)
    {
        $news = [];
        foreach ($newsDOs as $newsDO) {
            $news_dto = new NewsDTO();

            // From Rss Entity
            $news_dto->setCategory($rssDO->getCategory() ? $rssDO->getCategory()->getName() : null);
            $news_dto->setRssName($rssDO->getName());

            // From News Entity
            $news_dto->setTitle($newsDO->getTitle());
            $news_dto->setDescription($newsDO->getDescription());
            $news_dto->setLink($newsDO->getLink());
            $news_dto->setImage($newsDO->getImage());
            $news_dto->setPubDate($newsDO->getPubDate());
            $news[] = $news_dto;
        }
        return $news;
    }

    // Comparator function used for order news
    function comparator($news1, $news2)
    {
        return $news1->getPubDate() < $news2->getPubDate();
    }
}

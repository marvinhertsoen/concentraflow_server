<?php

namespace App\Model\DTO;

class FeedDTO
{
    public $news = [];

    /**
     * @return array
     */
    public function getNews(): array
    {
        return $this->news;
    }

    /**
     * @param array $news
     */
    public function setNews(array $news): void
    {
        $this->news = $news;
    }
}

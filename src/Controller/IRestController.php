<?php

namespace App\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Interface IRestController
 * @package App\Controller
 */
Interface IRestController
{

    public function getAllAction(): ?Response;

    public function getAction(Request $request): ?Response;

    public function newAction(Request $request): ?Response;

    public function editAction(Request $request): ?Response;

    public function deleteAction(Request $request): ?Response;

}

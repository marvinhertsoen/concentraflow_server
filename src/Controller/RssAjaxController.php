<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Rss;
use App\Model\DAO\RssDAO;
use App\Model\Service\ReadRssBo;
use App\Model\Service\RssBo;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RssAjaxController
 * @package App\Controller
 * @Route ("/api", name="api_")
 */
class RssAjaxController extends FOSRestController implements IRestController
{
    /**
     * @var ReadRssBo
     */
    private $reader;

    /**
     * @var RssBo
     */
    private $rssBo;

    public function __construct(
        RssBo $rssBo,
        ReadRssBo $reader
    )
    {
        $this->reader = $reader;
        $this->rssBo = $rssBo;
    }


    /**
     * List all Rss flows
     * @Rest\Get("/rss")
     * @return Response|null
     */
    public function getAllAction(): ?Response
    {

        $result = $this->reader->read("http://www.20minutes.fr/rss/actu-france.xml");
        return $this->handleView($this->view($result));


        $result = $this->rssBo->getAll();
        return $this->handleView($this->view($result));
    }

    /**
     * Get Rss by id
     * @Rest\Get("/rss/{id}")
     * @param Request $request
     * @return Response|null
     */
    public function getAction(Request $request): ?Response
    {
        $id = $request->get('id');
        $result = $this->rssBo->get($id);
        return $this->handleView($this->view($result));
    }

    /**
     * Create a Category Entity
     * @Rest\Post("/rss")
     * @param $request
     * @return Response
     */
    public function newAction(Request $request): ?Response
    {
        $data = json_decode($request->getContent(), true);
        $result = $this->rssBo->create($data);
        return $this->handleView($this->view($result));
    }

    /**
     * Update a Category Entity
     * @Rest\Put("/rss/{id}")
     * @param $request
     * @return Response
     */
    public function editAction(Request $request): ?Response
    {
        $id = $request->get('id');
        $data = json_decode($request->getContent(), true);
        $result = $this->rssBo->edit($id, $data);
        return $this->handleView($this->view($result));
    }

    /**
     * Remove a Rss Entity
     * @Rest\Delete("/rss/{id}")
     * @param $request
     * @return Response
     */
    public function deleteAction(Request $request): ?Response
    {
        $id = $request->get('id');
        $result = $this->rssBo->remove($id);
        return $this->handleView($this->view($result));
    }
}

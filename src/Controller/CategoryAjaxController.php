<?php

namespace App\Controller;

use App\Entity\Category;
use App\Model\Service\CategoryBo;
use App\Model\Service\ReadRssBo;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\Get;


/**
 * Class CategoryAjaxController
 * @package App\Controller
 * @Route ("/api", name="api_test")
 */
class CategoryAjaxController extends FOSRestController implements IRestController
{
    private $categoryBo;

    public function __construct(CategoryBo $categoryBo)
    {
        $this->categoryBo = $categoryBo;
    }

    /**
     * List all Category Entities
     * @Rest\Get("/category")
     * @return Response
     */
    public function getAllAction(): ?Response
    {
        $result = $this->categoryBo->getAll();
        return $this->handleView($this->view($result)->setStatusCode(Response::HTTP_OK));
    }


    /**
     * get Category by id
     * @param Request $request
     * @return Response|null
     */
    public function getAction(Request $request): ?Response
    {
    }


    /**
     * Create a Category Entity
     * @Rest\Post("/category")
     * @param $request
     * @return Response
     */
    public function newAction(Request $request): ?Response
    {
        $data = json_decode($request->getContent(), true);
        $result = $this->categoryBo->create($data);
        return $this->handleView($this->view($result));
    }

    /**
     * Create a Category Entity
     * @Rest\Put("/category/{id}")
     * @param $request
     * @return Response
     */
    public function editAction(Request $request): ?Response
    {
        $id = $request->get('id');
        $data = json_decode($request->getContent(), true);
        $result = $this->categoryBo->edit($id, $data);
        return $this->handleView($this->view($result));
    }

    /**
     * Remove a Category Entity
     * @Rest\Delete("/category/{id}")
     * @param $request
     * @return Response
     */
    public function deleteAction(Request $request): ?Response
    {
        $id = $request->get('id');
        $statusCode = ($this->categoryBo->remove($id)) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
        return $this->handleView($this->view()->setStatusCode($statusCode));
    }


    /**
     * Test
     * @Rest\Get("/test")
     * @param $request
     * @return Response
     */
    public function testAction(Request $request)
    {
        $test = "test";
        return $this->handleView($this->view($test));
    }

}

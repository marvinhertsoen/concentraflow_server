<?php

namespace App\Controller;

use App\Entity\Category;
use App\Model\Service\CategoryBo;
use App\Model\Service\FeedBo;
use App\Model\Service\ReadRssBo;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\Get;


/**
 * Class FeedAjaxController
 * @package App\Controller
 * @Route ("/api", name="api_test")
 */
class FeedAjaxController extends FOSRestController
{
    private $feedBo;

    public function __construct(FeedBo $feedBo)
    {
        $this->feedBo = $feedBo;
    }

    /**
     * Get the all feed
     * @Rest\Get("/feed")
     * @return Response
     * @throws \Exception
     */
    public function getAllAction(): ?Response
    {
        $result = $this->feedBo->getAll();
        return $this->handleView($this->view($result)->setStatusCode(Response::HTTP_OK));
    }


    /**
     * Get feed from single Rss feed
     * @Rest\Get("/feed/{id}")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function getAction(Request $request): ?Response
    {
        $id = $request->get('id');
        $result = $this->feedBo->get($id);
        return $this->handleView($this->view($result));
    }

    /**
     * Get feed from a Category
     * @Rest\Get("/feed/category/{id}")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function getFromCategoryAction(Request $request): ?Response
    {
        $id = $request->get('id');
        $result = $this->feedBo->getFromCategory($id);

        return $this->handleView($this->view($result));
    }

}

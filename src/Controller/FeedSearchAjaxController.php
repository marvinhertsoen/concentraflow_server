<?php

namespace App\Controller;

use App\Entity\Category;
use App\Model\Service\CategoryBo;
use App\Model\Service\FeedBo;
use App\Model\Service\FeedSearchBO;
use App\Model\Service\ReadRssBo;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\Get;


/**
 * Class FeedSearchAjaxController
 * @package App\Controller
 * @Route ("/api", name="api_test")
 */
class FeedSearchAjaxController extends FOSRestController
{
    /**
     * @var FeedSearchBO
     */
    private $feedSearchBo;

    /**
     * FeedSearchAjaxController constructor.
     * @param FeedSearchBO $feedSearchBo
     */
    public function __construct(FeedSearchBO $feedSearchBo)
    {
        $this->feedSearchBo = $feedSearchBo;
    }

    /**
     * Search feeds from a term
     * @Rest\Get("/feedsearch/{term}")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function searchAction(Request $request): ?Response
    {
        $term = $request->get('term');
        $term = urlencode($term);
        $result = $this->feedSearchBo->search($term);
        return $this->handleView($this->view($result));
    }

}
